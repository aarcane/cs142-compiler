package crux.automata;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

/***
 * 
 * @author aarcane
 *
 *	Implements a regex NDFA for parsing languages.  Should be essentially fully featured when complete.
 *	
 *	NOTE 1:	While implemented as a full NDFA with REGEX', this should also be fully functional as a DFA
 *			if you simply omit EPSILON transitions, only ever add a single target state per state/input
 *			pair and only set single character literals for your REGEX patterns.  No attempt will be made
 *			to enforce these constraints at design time.
 *
 *	NOTE 2:	While this is implemented using REGEX', it only matches a single character per step, so you
 *			don't get the full effect of REGEX'.  You can use this only for basic range and macro matching,
 *			like \d, \D, \w, etc.  Matches like \b and even simple dictionary words won't function at all,
 *			and are useless.  From the perspective of this documentation, their functionality is undefined.
 *			
 *	To use:
 *		1)	Construct a new NDFA.
 *		2)	Add a set of state transitions using addTransition.
 *		3)	Specify a start state.  If you do NOT specify a start state, the NDFA *WILL* fail to function.
 *		4)	Add a set of accepting States.
 *		5)	Call reset() on the automata.  This will call the initialization routines, and prime the
 *				NDFA for first and subsequent uses.
 */
public class regexNDFA extends Automata
{	private HashMap<Integer, HashMap<String, HashSet<Integer>>> stateList;
	private HashSet<Integer> currentStates;
	private HashSet<Integer> acceptingStates;
	private Integer startState;
	public static final String EPSILON = "_&_EPSILON_&_38769_&_"; //very probably won't match a defined keyword or user defined constant.

	public void addTransition(int src, String regex, int dst)
	{	if(!stateList.containsKey(src))
			stateList.put(src, new HashMap<String,HashSet<Integer>>());
		HashMap<String, HashSet<Integer>> subMap = stateList.get(src);
		if(!subMap.containsKey(regex))
			subMap.put(regex, new HashSet<Integer>());
		HashSet<Integer> subDst = subMap.get(regex);
		subDst.add(dst);
	}
	public void setStartState(int start) { startState = start; }
	public void addAcceptingState (int state) { acceptingStates.add(state); }
	
	public regexNDFA()
	{	super();
		stateList = new HashMap<Integer, HashMap<String, HashSet<Integer>>>();
		acceptingStates = new HashSet<Integer>();
	}
	
	@Override
	protected void runLogic(String next)
	{	HashSet<Integer> nextStates = new HashSet<Integer>();
		for(int s: currentStates)
			nextStates.addAll(getTransitionList(s, next));
		currentStates = nextStates;
		handleEpsilonTransitions();
		updateMachineState();
	}

	@Override
	protected void local_reset()
	{	currentStates = new HashSet<Integer>();
		currentStates.add(startState);
		handleEpsilonTransitions();
	}

	private void handleEpsilonTransitions()
	{	Boolean changed;
		do
		{	changed = false;
			for(Integer s : currentStates)
				changed = changed | currentStates.addAll(getTransitionList(s, EPSILON));
		} while(changed);
	}
	
	private HashSet<Integer> getTransitionList(int start, String trans)
	{	HashSet<Integer> ret = new HashSet<Integer>();
		HashMap<String, HashSet<Integer>> possibleTransitions = stateList.get(start);
		for(Entry<String, HashSet<Integer>> S: possibleTransitions.entrySet())
			if(trans.matches(S.getKey())) ret.addAll(S.getValue());
		return ret;
	}
	
	private void updateMachineState()
	{	if(currentStates.isEmpty())
			S = automataState.dead;
		else
		{	S = automataState.alive;
			for(Integer s: currentStates)
				if(acceptingStates.contains(s)) S = automataState.accepting;
		}
	}
}