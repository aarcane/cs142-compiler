package crux.automata;

public class StringAutomata extends Automata
{	private final String myString;
	private String inbound;
	public StringAutomata (String myString)
	{	super();
		this.myString = myString;
		inbound = "";
	}
	@Override
	protected void runLogic(String next)
	{	inbound += next;
		if(myString.equals(inbound))
			S = automataState.accepting;
		else if(myString.startsWith(inbound))
			S = automataState.alive;
		else
			S = automataState.dead;
	}
	@Override
	protected void local_reset() { inbound = ""; }
}
