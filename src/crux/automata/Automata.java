package crux.automata;

public abstract class Automata
{	public automataState S;
	Automata() { S = automataState.alive; }
	public enum automataState { dead, alive, accepting }
	public automataState step(String next)
	{	if(S != automataState.dead)
			runLogic(next);
		return S;
	}
	protected abstract void runLogic(String next);
	public void reset()
	{	S = automataState.alive;
		local_reset();
	}
	protected abstract void local_reset();
}

