package crux;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

public class SymbolTable {
    Stack<HashMap<String, Symbol>> ST;
    Stack<List<String>> STL;
    public SymbolTable()
    {	HashMap<String, Symbol> BuiltInSymbols = new HashMap<String, Symbol>();
    	List<String> BuiltInSymbolsOrdered = new ArrayList<String>();
    	ST = new Stack<HashMap<String, Symbol>>();
    	STL = new Stack<List<String>>();
    	
    	BuiltInSymbolsOrdered.add("readInt");
    	BuiltInSymbolsOrdered.add("readFloat");
    	BuiltInSymbolsOrdered.add("printBool");
    	BuiltInSymbolsOrdered.add("printInt");
    	BuiltInSymbolsOrdered.add("printFloat");
    	BuiltInSymbolsOrdered.add("println");
    	
    	for(String Sym: BuiltInSymbolsOrdered)
    		BuiltInSymbols.put(Sym,new Symbol(Sym));
    	
    	STL.push(BuiltInSymbolsOrdered);
    	ST.push(BuiltInSymbols);
    }
    public void push()
    {	ST.push(new HashMap<String,Symbol>());
    	STL.push(new ArrayList<String>());
    }
    public void pop()
    {	ST.pop();
    	STL.pop();
    }
    public Symbol lookup(String name) throws SymbolNotFoundError
    {	for(HashMap<String, Symbol> Sym: ST)
    	{	Symbol ret = Sym.get(name);
    		if(ret != null)
    			return ret;
    	}
        throw new SymbolNotFoundError(name);
    }
       
    public Symbol insert(String name) throws RedeclarationError
    {	Symbol S = new Symbol(name);
    	if(ST.peek().put(name, S) != null)
    		throw new RedeclarationError(S);
    	return S;
    }
    
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        Iterator<HashMap<String, Symbol>>si = ST.iterator();
        Iterator<List<String>>sti = STL.iterator();
        
        String indent = new String();

        
        do
        {	HashMap<String, Symbol> M = si.next();
        	List<String> L = sti.next();
        	for(String s: L)
        		sb.append(indent + M.get(s).toString() + "\n");
        	indent += "  ";
        	
        } while(si.hasNext() && sti.hasNext()); //be a bit pedantic, but it should catch a class of errors...
        
        return sb.toString();
        
        
/*      StringBuffer sb = new StringBuffer();       
        if (I have a parent table)
            sb.append(parent.toString());
        
        String indent = new String();
        for (int i = 0; i < depth; i++) {
            indent += "  ";
        }
        
        for (Every symbol, s, in this table)
        {
            sb.append(indent + s.toString() + "\n");
        }
        return sb.toString();
*/    }
}

class SymbolNotFoundError extends Error
{
    private static final long serialVersionUID = 1L;
    private String name;
    
    SymbolNotFoundError(String name)
    {
        this.name = name;
    }
    
    public String name()
    {
        return name;
    }
}

class RedeclarationError extends Error
{
    private static final long serialVersionUID = 1L;

    public RedeclarationError(Symbol sym)
    {
        super("Symbol " + sym + " being redeclared.");
    }
}
