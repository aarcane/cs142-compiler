package crux;

import crux.automata.Automata;
import crux.automata.StringAutomata;
import crux.automata.regexNDFA;
import java.util.ArrayList;
import java.util.HashMap;

public class Token {
	static public HashMap<Kind, Automata> au;
	static public ArrayList<Kind> k;
	static 
	{	k = new ArrayList<Kind>();
		k.add(Kind.AND);
		k.add(Kind.OR);
		k.add(Kind.NOT);
		k.add(Kind.LET);
		k.add(Kind.VAR);
		k.add(Kind.ARRAY);
		k.add(Kind.FUNC);
		k.add(Kind.IF);
		k.add(Kind.ELSE);
		k.add(Kind.WHILE);
		k.add(Kind.TRUE);
		k.add(Kind.FALSE);
		k.add(Kind.RETURN);
		k.add(Kind.OPEN_PAREN);
		k.add(Kind.CLOSE_PAREN);
		k.add(Kind.OPEN_BRACE);
		k.add(Kind.CLOSE_BRACE);
		k.add(Kind.OPEN_BRACKET);
		k.add(Kind.CLOSE_BRACKET);
		k.add(Kind.ADD);
		k.add(Kind.SUB);
		k.add(Kind.MUL);
		k.add(Kind.DIV);
		k.add(Kind.GREATER_EQUAL);
		k.add(Kind.LESSER_EQUAL);
		k.add(Kind.NOT_EQUAL);
		k.add(Kind.EQUAL);
		k.add(Kind.GREATER_THAN);
		k.add(Kind.LESS_THAN);
		k.add(Kind.ASSIGN);
		k.add(Kind.COMMA);
		k.add(Kind.SEMICOLON);
		k.add(Kind.COLON);
		k.add(Kind.CALL);

		au = new HashMap<Kind, Automata>();
		for(Kind l: k)
			au.put(l, new StringAutomata(l.default_lexeme));

		k.add(Kind.INTEGER);
		k.add(Kind.FLOAT);
		k.add(Kind.IDENTIFIER);
		
		regexNDFA r = new regexNDFA();
			r.addTransition(0, "\\d", 1);
			r.addTransition(1, "\\d", 1);
			r.setStartState(0);
			r.addAcceptingState(1);
		au.put(Kind.INTEGER, r);

		r = new regexNDFA();
			r.addTransition(0, "\\d", 1);
			r.addTransition(1, "\\d", 1);
			r.addTransition(1, "\\.", 2);
			r.addTransition(2, "\\d", 2);
			r.setStartState(0);
			r.addAcceptingState(2);
		au.put(Kind.FLOAT, r);

		r = new regexNDFA();
			r.addTransition(0, "[_a-zA-Z]", 1);
			r.addTransition(1, "[_a-zA-Z0-9]", 1);
			r.setStartState(0);
			r.addAcceptingState(1);
		au.put(Kind.IDENTIFIER, r);
		
	}
	public static enum Kind {
		AND("and"),
		OR("or"),
		NOT("not"),
		LET("let"),
		VAR("var"),
		ARRAY("array"),
		FUNC("func"),
		IF("if"),
		ELSE("else"),
		WHILE("while"),
		TRUE("true"),
		FALSE("false"),
		RETURN("return"),
		
		OPEN_PAREN("("),
		CLOSE_PAREN(")"),
		OPEN_BRACE("{"),
		CLOSE_BRACE("}"),
		OPEN_BRACKET("["),
		CLOSE_BRACKET("]"),
		ADD("+"),
		SUB("-"),
		MUL("*"),
		DIV("/"),
		GREATER_EQUAL(">="),
		LESSER_EQUAL("<="),
		NOT_EQUAL("!="),
		EQUAL("=="),
		GREATER_THAN(">"),
		LESS_THAN("<"),
		ASSIGN("="),
		COMMA(","),
		SEMICOLON(";"),
		COLON(":"),
		CALL("::"),
		
		INTEGER(),
		FLOAT(),
		IDENTIFIER(),
		ERROR(),
		EOF();
		
		private String default_lexeme;
		
		Kind()
		{
			default_lexeme = null;
		}
		
		Kind(String lexeme)
		{
			default_lexeme = lexeme;
		}
		
		public boolean hasStaticLexeme()
		{
			return default_lexeme != null;
		}
		
		// OPTIONAL: if you wish to also make convenience functions, feel free
		//		   for example, boolean matches(String lexeme)
		//		   can report whether a Token.Kind has the given lexeme
	}
	
	private int lineNum;
	private int charPos;
	private Kind kind;
	private String lexeme = "";
	
	
	// OPTIONAL: implement factory functions for some tokens, as you see fit
	public static Token EOF(int linePos, int charPos)
	{
		Token tok = new Token(linePos, charPos);
		tok.kind = Kind.EOF;
		return tok;
	}
	
	public static Token ERROR(String ch, int linePos, int charPos)
	{
		Token tok = new Token(linePos, charPos);
		tok.lexeme = ch;
		tok.kind = Kind.ERROR;
		return tok;
	}


	private Token(int lineNum, int charPos)
	{
		this.lineNum = lineNum;
		this.charPos = charPos;
		
		// if we don't match anything, signal error
		this.kind = Kind.ERROR;
		this.lexeme = "No Lexeme Given";
	}
	
	public Token(String lexeme, Kind kind, int lineNum, int charPos)
	{
		this.lineNum = lineNum;
		this.charPos = charPos;
		
		this.kind = kind;
		this.lexeme = lexeme;
	}
	
	public int lineNumber()
	{	return lineNum;
	}
	
	public int charPosition()
	{	return charPos;
	}
	
	// Return the lexeme representing or held by this token
	public String lexeme()
	{	return lexeme;
	}
	
	public String toString()
	{	String S = "";
		S += kind.name();
		if(kind == Kind.ERROR) S += "(Unexpected character: " + lexeme + ")";
		if(kind == Kind.INTEGER | kind == Kind.FLOAT | kind == Kind.IDENTIFIER) S += "(" + lexeme + ")";
		S += "(lineNum:";
		S += Integer.toString(lineNumber());
		S += ", charPos:";
		S += Integer.toString(charPosition());
		S += ")";
		return S;
	}
	
	public Kind kind()
	{
		return this.kind;
	}
	// OPTIONAL: function to query a token about its kind
	//		   boolean is(Token.Kind kind)
	public Boolean is(Token.Kind K)
	{	return (K == kind);
	}
	
	// OPTIONAL: add any additional helper or convenience methods
	//		   that you find make for a clean design

}
