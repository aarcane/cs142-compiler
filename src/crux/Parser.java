package crux;

import java.util.Stack;
import crux.Token;
import crux.Token.Kind;

public class Parser
{	public static final String studentName = "Christ Schlacta";
	public static final String studentID = "32362822";
	public static final String uciNetID = "cschlact";

// Grammar Rule Reporting ==========================================
	//private int parseTreeRecursionDepth = 0;
	private StringBuffer parseTreeBuffer = new StringBuffer();
	private Stack<NonTerminal> parseTreeStack = new Stack<NonTerminal>();

	public String parseTreeReport()
	{	return parseTreeBuffer.toString();
	}
	
	private void stringError() {};

// Error Reporting ==========================================
	private StringBuffer errorBuffer = new StringBuffer();

	private String reportSyntaxError(NonTerminal nt)
	{	String message = "SyntaxError(" + lineNumber() + "," + charPosition() + ")[Expected a token from " + nt.name() + " but got " + currentToken.kind() + ".]";
		errorBuffer.append(message + "\n");
		return message;
	}

	private String reportSyntaxError(Token.Kind kind)
	{	String message = "SyntaxError(" + lineNumber() + "," + charPosition() + ")[Expected " + kind + " but got " + currentToken.kind() + ".]";
		errorBuffer.append(message + "\n");
		return message;
	}
	
	boolean enterRule(NonTerminal nt) {return true;};
	boolean enterRule() {return true;};
	boolean exitRule(NonTerminal nt) {return true;};
	boolean exitRule() {return true;};
	public String errorReport()
	{	return errorBuffer.toString();
	}

	public boolean hasError()
	{	return errorBuffer.length() != 0;
	}

	private class QuitParseException extends RuntimeException
	{	private static final long serialVersionUID = 1L;
		public QuitParseException(String errorMessage)
		{	super(errorMessage);
		}
	}

	private int lineNumber()
	{	return currentToken.lineNumber();
	}

	private int charPosition()
	{	return currentToken.charPosition();
	}
	// SymbolTable Management ==========================
    private SymbolTable ST;
    
    private void initSymbolTable()
    {	ST = new SymbolTable();
    }
    
    private void enterScope()
    {	ST.push();
    }
    
    private void exitScope()
    {	ST.pop();
    }

    private Symbol tryResolveSymbol(Token ident)
    {
        assert(ident.is(Token.Kind.IDENTIFIER));
        String name = ident.lexeme();
        try {
            return ST.lookup(name);
        } catch (SymbolNotFoundError e) {
            String message = reportResolveSymbolError(name, ident.lineNumber(), ident.charPosition());
            return new ErrorSymbol(message);
        }
    }

    private String reportResolveSymbolError(String name, int lineNum, int charPos)
    {
        String message = "ResolveSymbolError(" + lineNum + "," + charPos + ")[Could not find " + name + ".]";
        errorBuffer.append(message + "\n");
        errorBuffer.append(ST.toString() + "\n");
        return message;
    }

    private Symbol tryDeclareSymbol(Token ident)
    {
        assert(ident.is(Token.Kind.IDENTIFIER));
        String name = ident.lexeme();
        try {
            return ST.insert(name);
        } catch (RedeclarationError re) {
            String message = reportDeclareSymbolError(name, ident.lineNumber(), ident.charPosition());
            return new ErrorSymbol(message);
        }
    }

    private String reportDeclareSymbolError(String name, int lineNum, int charPos)
    {
        String message = "DeclareSymbolError(" + lineNum + "," + charPos + ")[" + name + " already exists.]";
        errorBuffer.append(message + "\n");
        errorBuffer.append(ST.toString() + "\n");
        return message;
    }    
// Parser ==========================================
	private Scanner scanner;
	private Token currentToken;

	public Parser(Scanner scanner)
	{	this.scanner = scanner;
		this.currentToken = scanner.next();
	}

	public void parse()
	{	try
		{	program();
		} catch (QuitParseException q)
		{	errorBuffer.append("SyntaxError(" + lineNumber() + "," + charPosition() + ")");
			errorBuffer.append("[Could not complete parsing.]");
		}
	}

// Helper Methods ==========================================
	private boolean have(Token.Kind kind)
	{	return currentToken.is(kind);
	}

	private boolean have(NonTerminal nt)
	{	return nt.firstSet().contains(currentToken.kind());
	}

	private boolean accept(Token.Kind kind)
	{	if (have(kind))
		{	currentToken = scanner.next();
			return true;
		}
		return false;
	}

	private boolean accept(NonTerminal nt)
	{	if (have(nt))
		{	currentToken = scanner.next();
			return true;
		}
		return false;
	}

	private boolean expect(Token.Kind kind)
	{	if (accept(kind))
			return true;
		String errorMessage = reportSyntaxError(kind);
		throw new QuitParseException(errorMessage);
		//return false;
	}

	private boolean expect(NonTerminal nt)
	{	if (accept(nt))
			return true;
		String errorMessage = reportSyntaxError(nt);
		throw new QuitParseException(errorMessage);
		//return false;
	}

// Grammar Rules =====================================================

	/***
	 * literal := INTEGER | FLOAT | TRUE | FALSE .
	 */
	public void literal()
	{	enterRule(NonTerminal.LITERAL);

		expect(NonTerminal.LITERAL);

		exitRule(NonTerminal.LITERAL);
	}

	/***
	 * designator := IDENTIFIER { "[" expression0 "]" } .
	 */
	public void designator()
	{	enterRule(NonTerminal.DESIGNATOR);

		expect(Token.Kind.IDENTIFIER);
		while (accept(Kind.OPEN_BRACKET))
		{	expression0();
			expect(Token.Kind.CLOSE_BRACKET);
		}

		exitRule(NonTerminal.DESIGNATOR);
	}

	/***
	 * type := IDENTIFIER .
	 */
	public void type()
	{	enterRule(NonTerminal.TYPE);

		expect(Kind.IDENTIFIER);

		exitRule(NonTerminal.TYPE);
	}

	/***
	 * op0 := ">=" | "<=" | "!=" | "==" | ">" | "<" .
	 */
	public void op0()
	{	enterRule(NonTerminal.OP0);

		expect(NonTerminal.OP0);

		exitRule(NonTerminal.OP0);
	}

	/***
	 * op1 := "+" | "-" | "or" .
	 */
	public void op1()
	{	enterRule(NonTerminal.OP1);

		expect(NonTerminal.OP1);

		exitRule(NonTerminal.OP1);
	}

	/***
	 * op2 := "*" | "/" | "and" .
	 */
	public void op2()
	{	enterRule(NonTerminal.OP2);

		expect(NonTerminal.OP2);

		exitRule(NonTerminal.OP2);
	}

	/***
	 * expression0 := expression1 [ op0 expression1 ] .
	 */
	public void expression0()
	{	enterRule(NonTerminal.EXPRESSION0);

		expression1();
		if(have(NonTerminal.OP0))
		{	op0();
			expression1();
		}

		exitRule(NonTerminal.EXPRESSION0);
	}

	/***
	 * expression1 := expression2 { op1  expression2 } .
	 */
	public void expression1()
	{	enterRule(NonTerminal.EXPRESSION1);

		expression2();
		while(have(NonTerminal.OP1))
		{	op1();
			expression2();
		}
		exitRule(NonTerminal.EXPRESSION1);
	}

	/***
	 * expression2 := expression3 { op2 expression3 } .
	 */
	public void expression2()
	{	enterRule(NonTerminal.EXPRESSION2);

		expression3();
		while(have(NonTerminal.OP2))
		{	op2();
			expression3();
		}
		exitRule(NonTerminal.EXPRESSION2);
	}

	/***
	 * expression3 := "not" expression3
	 *	| "(" expression0 ")"
	 *	| designator
	 *	| call-expression
	 *	| literal .
	 */
	public void expression3()
	{	enterRule(NonTerminal.EXPRESSION3);

		if(accept(Kind.NOT))
			expression3();
		else if(accept(Kind.OPEN_PAREN))
		{	expression0();
			expect(Kind.CLOSE_PAREN);
		}
		else if(have(NonTerminal.DESIGNATOR))
			designator();
		else if(have(NonTerminal.CALL_EXPRESSION))
			call_expression();
		else if(have(NonTerminal.LITERAL))
			literal();

		exitRule(NonTerminal.EXPRESSION3);
	}

	/***
	 * call-expression := "::" IDENTIFIER "(" expression-list ")" .
	 */
	public void call_expression()
	{	enterRule(NonTerminal.CALL_EXPRESSION);

		expect(Kind.CALL);
		expect(Kind.IDENTIFIER);
		expect(Kind.OPEN_PAREN);
		expression_list();
		expect(Kind.CLOSE_PAREN);

		exitRule(NonTerminal.CALL_EXPRESSION);
	}
	/***
	 * expression-list := [ expression0 { "," expression0 } ] .
	 */
	public void expression_list()
	{	enterRule(NonTerminal.EXPRESSION_LIST);

		if(have(NonTerminal.EXPRESSION0))
			do
				expression0();
			while(accept(Kind.COMMA));

		exitRule(NonTerminal.EXPRESSION_LIST);
	}

	/***
	 * parameter := IDENTIFIER ":" type .
	 */
	public void parameter()
	{	enterRule(NonTerminal.PARAMETER);

		expect(Kind.IDENTIFIER);
		expect(Kind.COLON);
		type();

		exitRule(NonTerminal.PARAMETER);
	}

	/***
	 * parameter-list := [ parameter { "," parameter } ] .
	 */
	public void parameter_list()
	{	enterRule(NonTerminal.PARAMETER_LIST);

		if(have(NonTerminal.PARAMETER))
			do
				parameter();
			while(accept(Kind.COMMA));

		exitRule(NonTerminal.PARAMETER_LIST);
	}

	/***
	 * variable-declaration := "var" IDENTIFIER ":" type ";"
	 */
	public void variable_declaration()
	{	enterRule(NonTerminal.VARIABLE_DECLARATION);

		expect(Kind.VAR);
		expect(Kind.IDENTIFIER);
		expect(Kind.COLON);
		type();
		expect(Kind.SEMICOLON);

		exitRule(NonTerminal.VARIABLE_DECLARATION);
	}

	/***
	 * array-declaration := "array" IDENTIFIER ":" type "[" INTEGER "]" { "[" INTEGER "]" } ";"
	 */
	public void array_declaration()
	{	expect(Kind.ARRAY);
		expect(Kind.IDENTIFIER);
		expect(Kind.COLON);
		type();
		do
		{	expect(Kind.OPEN_BRACKET);
			expect(Kind.INTEGER);
			expect(Kind.CLOSE_BRACKET);
		} while(have(Kind.OPEN_BRACKET));
		expect(Kind.SEMICOLON);
	}

	/***
	 * function-definition := "func" IDENTIFIER "(" parameter-list ")" ":" type statement-block .
	 */
	public void function_definition()
	{	expect(Kind.FUNC);
		expect(Kind.IDENTIFIER);
		expect(Kind.OPEN_PAREN);
		enterScope();
			parameter_list();
			expect(Kind.CLOSE_PAREN);
			expect(Kind.COLON);
			type();
			statement_block();
		exitScope();
	}

	/***
	 * declaration := variable-declaration | array-declaration | function-definition .
	 */
	public void declaration()
	{	if(have(NonTerminal.VARIABLE_DECLARATION))
			variable_declaration();
		else if(have(NonTerminal.ARRAY_DECLARATION))
			array_declaration();
		else if(have(NonTerminal.FUNCTION_DEFINITION))
			function_definition();
	}

	/***
	 * declaration-list := { declaration } .
	 */
	public void declaration_list()
	{	while(have(NonTerminal.DECLARATION))
			declaration();

	}

	/***
	 * assignment-statement := "let" designator "=" expression0 ";"
	 */
	public void assignment_statement()
	{	designator();
		expect(Kind.ASSIGN);
		expression0();
		expect(Kind.SEMICOLON);

		}

	/***
	 * call-statement := call-expression ";"
	 */
	public void call_statement()
	{	
		call_expression();
		expect(Kind.SEMICOLON);

		}

	/***
	 * if-statement := "if" expression0 statement-block [ "else" statement-block ] .
	 */
	public void if_statement()
	{	expect(Kind.IF);
		expression0();
		enterScope();
			statement_block();
		exitScope();
		if(accept(Kind.ELSE))
		{	enterScope();
				statement_block();
			exitScope();
		}
	}

	/***
	 * while-statement := "while" expression0 statement-block .
	 */
	public void while_statement()
	{	expect(Kind.WHILE);
		expression0();
		enterScope();
			statement_block();
		exitScope();
	}

	/***
	 * return-statement := "return" expression0 ";" .
	 */
	public void return_statement()
	{	enterRule(NonTerminal.RETURN_STATEMENT);

		expect(Kind.RETURN);
		expression0();
		expect(Kind.SEMICOLON);

		exitRule();
	}

	/***
	 * 	statement := variable-declaration
	 *	| call-statement
	 *	| assignment-statement
	 *	| if-statement
	 *	| while-statement
	 *	| return-statement .
	 */
	public void statement()
	{	enterRule(NonTerminal.STATEMENT);

		if(have(NonTerminal.VARIABLE_DECLARATION))
			variable_declaration();
		else if(have(NonTerminal.CALL_STATEMENT))
			call_statement();
		else if(have(NonTerminal.ASSIGNMENT_STATEMENT))
			assignment_statement();
		else if(have(NonTerminal.IF_STATEMENT))
			if_statement();
		else if(have(NonTerminal.WHILE_STATEMENT))
			while_statement();
		else if(have(NonTerminal.RETURN_STATEMENT))
			return_statement();

		exitRule(NonTerminal.STATEMENT);
	}

	/***
	 * statement-list := { statement } .
	 */
	public void statement_list()
	{	enterRule(NonTerminal.STATEMENT_LIST);

		while(have(NonTerminal.STATEMENT))
			statement();

		exitRule(NonTerminal.STATEMENT_LIST);
	}

	/***
	 * statement-block := "{" statement-list "}" .
	 */
	public void statement_block()
	{	enterRule(NonTerminal.STATEMENT_BLOCK);

		expect(Kind.OPEN_BRACE);
		statement_list();
		expect(Kind.CLOSE_BRACE);

		exitRule(NonTerminal.STATEMENT_BLOCK);
	}

	/***
	 * program := declaration-list EOF .
	 */
	public void program()
	{	enterRule(NonTerminal.PROGRAM);

		if(have(NonTerminal.DECLARATION_LIST))
			declaration_list();
		expect(Kind.EOF);

		exitRule(NonTerminal.PROGRAM);
	}
}
