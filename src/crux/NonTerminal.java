package crux;
import java.util.HashSet;
import java.util.Set;
import crux.Token.Kind;

public enum NonTerminal {

	// TODO: mention that we are not modeling the empty string
	// TODO: mention that we are not doing a first set for every line in the grammar
	//       some lines have already been handled by the CruxScanner
	
	DESIGNATOR(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	add(Kind.IDENTIFIER);
		}}),
	TYPE(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	add(Kind.IDENTIFIER);
		}}),
	LITERAL(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	add(Kind.INTEGER);
			add(Kind.FLOAT);
			add(Kind.TRUE);
			add(Kind.FALSE);
		}}),
	CALL_EXPRESSION(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	add(Kind.CALL);
		}}),
	OP0(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	add(Kind.GREATER_EQUAL);
			add(Kind.LESSER_EQUAL);
			add(Kind.NOT_EQUAL);
			add(Kind.EQUAL);
			add(Kind.GREATER_THAN);
			add(Kind.LESS_THAN);
		}}),
	OP1(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	add(Kind.ADD);
			add(Kind.SUB);
			add(Kind.OR);
		}}),
	OP2(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	add(Kind.MUL);
			add(Kind.DIV);
			add(Kind.AND);
		}}),
	EXPRESSION3(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	add(Kind.NOT);
			add(Kind.OPEN_PAREN);
			addAll(DESIGNATOR.firstSet());
			addAll(CALL_EXPRESSION.firstSet());
			addAll(LITERAL.firstSet());
		}}),
	EXPRESSION2(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	addAll(EXPRESSION3.firstSet());
		}}),
	EXPRESSION1(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	addAll(EXPRESSION2.firstSet());
		}}),
	EXPRESSION0(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	addAll(EXPRESSION1.firstSet());
		}}),
	EXPRESSION_LIST(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	addAll(EXPRESSION0.firstSet());
		}}),
	PARAMETER(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	add(Kind.IDENTIFIER);
		}}),
	PARAMETER_LIST(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	addAll(PARAMETER.firstSet());
		}}),
	VARIABLE_DECLARATION(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	add(Kind.VAR);
		}}),
	ARRAY_DECLARATION(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	add(Kind.ARRAY);
		}}),
	FUNCTION_DEFINITION(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	add(Kind.FUNC);
		}}),
	DECLARATION(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	addAll(VARIABLE_DECLARATION.firstSet());
			addAll(ARRAY_DECLARATION.firstSet());
			addAll(FUNCTION_DEFINITION.firstSet());
		}}),
	DECLARATION_LIST(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	addAll(DECLARATION.firstSet());
		}}),	
	ASSIGNMENT_STATEMENT(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	add(Kind.LET);
		}}),
	CALL_STATEMENT(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	addAll(CALL_EXPRESSION.firstSet());
		}}),
	IF_STATEMENT(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	add(Kind.IF);
		}}),
	WHILE_STATEMENT(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	add(Kind.WHILE);
		}}),
	RETURN_STATEMENT(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	add(Kind.RETURN);
		}}),
	STATEMENT_BLOCK(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	add(Kind.OPEN_BRACE);
		}}),
	STATEMENT(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	final NonTerminal sb[] = {VARIABLE_DECLARATION, CALL_STATEMENT, ASSIGNMENT_STATEMENT, IF_STATEMENT, WHILE_STATEMENT, RETURN_STATEMENT};
			for (NonTerminal n: sb) addAll(n.firstSet());
		}}),
	STATEMENT_LIST(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	addAll(STATEMENT.firstSet());
		}}),
	PROGRAM(new HashSet<Token.Kind>() {
		private static final long serialVersionUID = 1L;
		{	addAll(DECLARATION_LIST.firstSet());
		}});

	public final HashSet<Token.Kind> firstSet = new HashSet<Token.Kind>();

	NonTerminal(HashSet<Token.Kind> t)
	{	firstSet.addAll(t);
	}

	public final Set<Token.Kind> firstSet()
	{	return firstSet;
	}
}
