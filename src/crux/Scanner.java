package crux;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;


import java.util.Map.Entry;
import java.lang.UnsupportedOperationException;

import crux.automata.Automata;

public class Scanner implements Iterable<Token> {
	public static String studentName = "Christ Schlacta";
	public static String studentID = "32362822";
	public static String uciNetID = "cschlact";

	private int lineNum;  // current line count
	private int charPos;  // character offset for current line
	private int nextChar; // contains the next char (-1 == EOF)
	private Reader input;
	//private 
	public Scanner(Reader reader)
	{	this.input = reader;
		lineNum = 1;
		charPos = 0;
		readChar();
		readChar(true);
	}	
	
	// OPTIONAL: helper function for reading a single char from input
	//		   can be used to catch and handle any IOExceptions,
	//		   advance the charPos or lineNum, etc.

	private int readChar()
	{	try
		{	nextChar = input.read();
			++charPos;
			//handle LF
			if(nextChar == Character.codePointAt("\n", 0))
			{	charPos = 0;
				++lineNum;
			}
		}
		catch (IOException E)  //No matter what goes wrong, we can't continue, so just bail here.
		{	nextChar = -1;
		}
		return nextChar;
	}
	private int readChar(Boolean clearWS)
	{	int oldLine = lineNum;
		if(clearWS)
			while(Character.isWhitespace(nextChar) && nextChar != -1)
				readChar();
		else
		{	while(lineNum == oldLine && nextChar != -1)
				readChar();
			readChar(true);
		}
		return nextChar;
	}

	/* Invariants:
	 *  1. call assumes that nextChar is already holding an unread character
	 *  2. return leaves nextChar containing an untokenized character
	 */
	public Token next()
	{	if(nextChar == -1)
			return Token.EOF(lineNum, charPos);
		
		int firstChar = nextChar;
		int startingLineNumber = lineNum;
		int startingCharPos = charPos;

		for(Automata a : Token.au.values())
			a.reset();
		
		Boolean AutomataAreAlive = true;
		String lexeme = "";
		String lastLexeme = "";
		Token.Kind lastKind = null; 
		while(AutomataAreAlive)
		{	String l = Character.toString((char)nextChar);
			for(Automata a: Token.au.values())
				a.step(l);
			AutomataAreAlive = false;
			for(Entry<Token.Kind, Automata> a : Token.au.entrySet())
				AutomataAreAlive = AutomataAreAlive | a.getValue().S.equals(Automata.automataState.alive) | a.getValue().S.equals(Automata.automataState.accepting);
			
			lexeme += l;
			
			if(lexeme.equals("//"))
			{	readChar(false);
				return next();
				/* I had started out by trying to just reset all the state, then break the loop...
				 * but when I actually realized how much work that would truly be...  I just decided
				 * to make it a recursive call instead.
				 * In the event that I somehow end up with a stack overflow,  I have a simple plan
				 * to wrap Token.next in a higher function, return null in this instance, and then
				 * call it again.
				 */
			}
			
			if(AutomataAreAlive)
				for(Token.Kind k : Token.k)
					if(Token.au.get(k).S.equals(Automata.automataState.accepting))
					{	lastLexeme = lexeme;
						lastKind = k;
						break;
					}
			if(AutomataAreAlive)
				readChar();
		}
		
		readChar(true);
		
		if(lastKind != null)
			return new Token(lastLexeme, lastKind, startingLineNumber, startingCharPos);
		
		if(nextChar - firstChar < 1)
			readChar();
		readChar(true);
		return Token.ERROR(Character.toString((char)firstChar), startingLineNumber, startingCharPos);

	}

	// OPTIONAL: any other methods that you find convenient for implementation or testing
	public boolean hasNext()
	{	return (nextChar > -1);
	}

	@Override
	public Iterator<Token> iterator() {
		return new TokenIt(this);
	}
	
	public class TokenIt implements Iterator<Token>
	{	private Scanner S;
		public TokenIt(Scanner S)
		{	this.S = S;
		}

		@Override
		public boolean hasNext()
		{	return S.hasNext();
		}

		@Override
		public Token next()
		{	return S.next();
		}
		
		@Override
		public void remove()
		{	throw new UnsupportedOperationException("Remove is not implemented for Token Stream");
		}
	}
}
