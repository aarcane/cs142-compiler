package crux;
import java.io.FileReader;
import java.io.IOException;

public class Compiler
{	public static final String studentName = "Christ Schlacta";
	public static final String studentID = "32362822";
	public static final String uciNetID = "cschlact";

	public static void main(String[] args)
	{	String sourceFilename = args[0];

		Scanner s = null;
		try {
			s = new Scanner(new FileReader(sourceFilename));
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Error accessing the source file: \"" + sourceFilename + "\"");
			System.exit(-2);
		}

		Parser p = new Parser(s);
		p.parse();
		if (p.hasError()) {
//			System.out.println(p.parseTreeReport());
//			System.out.println();
//			System.out.println();
			System.out.println("Error parsing file.");
			System.out.println(p.errorReport());
			System.exit(-3);

		}
		System.out.println(p.parseTreeReport());
	}
}
